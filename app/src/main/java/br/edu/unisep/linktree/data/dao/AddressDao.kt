package br.edu.unisep.linktree.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import br.edu.unisep.linktree.data.entity.Address

@Dao
interface AddressDao {

    @Insert
    suspend fun save(link: Address)

    @Update
    suspend fun update(link: Address)

    @Query("select * from address")
    suspend fun findAll(): List<Address>

    @Query("select * from address where id = :id")
    suspend fun findById(id: Int) : Address

    @Query("select * from address where title like :title")
    suspend fun findByTitle(title: String): List<Address>
}