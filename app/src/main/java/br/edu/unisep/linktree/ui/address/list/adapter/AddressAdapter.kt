package br.edu.unisep.linktree.ui.address.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.linktree.R
import br.edu.unisep.linktree.databinding.ActivityMainBinding
import br.edu.unisep.linktree.databinding.DialogNewLinkBinding
import br.edu.unisep.linktree.databinding.LinkItemBinding
import br.edu.unisep.linktree.domain.dto.address.AddressDto

class AddressAdapter(private val onItemSelected: (AddressDto) -> Unit)
    : RecyclerView.Adapter<AddressAdapter.LinkViewHolder>() {

    var links: List<AddressDto> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = LinkItemBinding.inflate(layoutInflater, parent, false)
        return LinkViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        holder.bind(links[position], onItemSelected)
    }

    override fun getItemCount() = links.size

    class LinkViewHolder(private val itemBinding: LinkItemBinding)
        : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(link: AddressDto, onItemSelected: (AddressDto) -> Unit) {
            itemBinding.tvAddressTitle.text = link.title
            itemBinding.tvAddressDescription.text = link.description
            itemBinding.root.setOnClickListener {
                onItemSelected(link)
            }
        }
    }
}