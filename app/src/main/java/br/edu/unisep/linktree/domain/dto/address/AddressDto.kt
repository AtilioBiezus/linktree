package br.edu.unisep.linktree.domain.dto.address

import java.io.Serializable

data class AddressDto(
    val id: Int,
    val title: String,
    val link: String,
    val description: String?,
) : Serializable
