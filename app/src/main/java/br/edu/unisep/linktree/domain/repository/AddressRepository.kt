package br.edu.unisep.linktree.domain.repository


import br.edu.unisep.linktree.data.dao.AddressDao
import br.edu.unisep.linktree.data.db.LinkTreeDb
import br.edu.unisep.linktree.data.entity.Address
import br.edu.unisep.linktree.domain.dto.address.AddressDto
import br.edu.unisep.linktree.domain.dto.address.NewAddressDto

class AddressRepository {

    private val dao: AddressDao by lazy {
        LinkTreeDb.instance.addressDao()
    }

    suspend fun save(newAddress: NewAddressDto) {
        val address = Address(
            newAddress.title,
            newAddress.link,
            newAddress.description
        )
        dao.save(address)
    }

    suspend fun findAll(): List<AddressDto> {
        val result = dao.findAll()
        return result.map { address ->
            AddressDto(
                address.id!!,
                address.title,
                address.link,
                address.description
            )
        }
    }

    suspend fun filterByTitle(title: String): List<AddressDto> {
        val result = dao.findByTitle("%${title}%")
        return result.map { address ->
            AddressDto(
                address.id!!,
                address.title,
                address.description,
                address.link
            )
        }
    }
}
