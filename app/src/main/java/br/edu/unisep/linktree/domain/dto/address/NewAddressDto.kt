package br.edu.unisep.linktree.domain.dto.address

data class NewAddressDto(
    val title: String,
    val link: String,
    val description: String
)