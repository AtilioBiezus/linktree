package br.edu.unisep.linktree.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.edu.unisep.linktree.data.dao.AddressDao
import br.edu.unisep.linktree.data.entity.Address

@Database(
    entities = [Address::class],
    version = 1
)
abstract class LinkTreeDb : RoomDatabase() {

    abstract fun addressDao() : AddressDao

    companion object {
        private lateinit var mInstance: LinkTreeDb
        val instance: LinkTreeDb
            get() = mInstance

        fun initialize(context: Context) {
            mInstance = Room.databaseBuilder(context,
                LinkTreeDb::class.java, "db_linktree").build()
        }
    }
}