package br.edu.unisep.linktree.ui.address.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import br.edu.unisep.linktree.databinding.DialogNewLinkBinding
import br.edu.unisep.linktree.domain.dto.address.NewAddressDto
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

typealias OnSaveLink = (NewAddressDto) -> Unit

class NewAddressDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogNewLinkBinding
    lateinit var onSave: OnSaveLink

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogNewLinkBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHandlers()
    }

    private fun setupHandlers() {
        binding.btnSave.setOnClickListener {
            save()
        }
    }

    private fun save() {
        val link = NewAddressDto(
            binding.etTitle.text.toString(),
            binding.etDescription.text.toString(),
            binding.etLink.text.toString()
        )
        onSave(link)
        dialog?.dismiss()
    }

    companion object {
        @JvmStatic
        fun createAndShow(fragmentManager: FragmentManager, onSave: OnSaveLink) {
            val dialog = NewAddressDialog()
            dialog.onSave = onSave
            dialog.show(fragmentManager, "new-link")
        }
    }
}
