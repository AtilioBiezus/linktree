package br.edu.unisep.linktree.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Address(
    val title: String,
    val description: String,
    val link: String
) {
    @PrimaryKey(autoGenerate = true) var id: Int? = null
}