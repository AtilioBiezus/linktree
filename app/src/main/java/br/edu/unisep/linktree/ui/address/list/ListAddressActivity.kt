package br.edu.unisep.linktree.ui.address.list

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import br.edu.unisep.linktree.R
import br.edu.unisep.linktree.databinding.ActivityMainBinding

import br.edu.unisep.linktree.domain.dto.address.AddressDto
import br.edu.unisep.linktree.domain.dto.address.NewAddressDto
import br.edu.unisep.linktree.ui.address.list.adapter.AddressAdapter
import br.edu.unisep.linktree.ui.address.list.viewmodel.ListAddressViewModel
import br.edu.unisep.linktree.ui.address.register.NewAddressDialog


class ListAddressActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<ListAddressViewModel>()

    private lateinit var adapter: AddressAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
        setupObservers()

        viewModel.findAll()
    }

    private fun setupView() {
        adapter = AddressAdapter(::onLinkSelect)

        binding.rvLinks.adapter = adapter
        binding.rvLinks.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvLinks.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        binding.etSearch.addTextChangedListener { text ->
            viewModel.filterByTitle(text.toString())
        }
    }

    private fun onLinkSelect(link: AddressDto) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.link))
        startActivity(browserIntent)
    }

    private fun setupObservers() {
        binding.root.setOnClickListener {
            binding.etSearch.clearFocus()
        }

        viewModel.links.observe(this) {
            adapter.links = it
        }
        viewModel.saveResult.observe(this) {
            viewModel.findAll()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_link, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnNew) {
            NewAddressDialog.createAndShow(supportFragmentManager, ::onSave)
            return true
        }

        return false
    }

    private fun onSave(link: NewAddressDto) {
        viewModel.save(link)
    }
}