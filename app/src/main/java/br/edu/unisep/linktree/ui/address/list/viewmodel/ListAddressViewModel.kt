package br.edu.unisep.linktree.ui.address.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.linktree.domain.dto.address.AddressDto
import br.edu.unisep.linktree.domain.dto.address.NewAddressDto
import br.edu.unisep.linktree.domain.repository.AddressRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListAddressViewModel : ViewModel() {

    private val _links = MutableLiveData<List<AddressDto>>()
    val links: LiveData<List<AddressDto>>
    get() = _links

    private val _saveResult = MutableLiveData<Unit>()
    val saveResult: LiveData<Unit>
    get() = _saveResult

    private val repository = AddressRepository()

    fun findAll() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.findAll()
            _links.postValue(list)
        }
    }

    fun save(link: NewAddressDto) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.save(link)
            _saveResult.postValue(Unit)
        }
    }

    fun filterByTitle(title: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.filterByTitle(title)
            _links.postValue(list)
        }
    }
}